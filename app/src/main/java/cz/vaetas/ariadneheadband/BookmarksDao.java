package cz.vaetas.ariadneheadband;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface BookmarksDao {
    @Query("SELECT * FROM Bookmark")
    List<Bookmark> getAll();

    @Insert
    void insert(Bookmark bookmark);

    @Query("DELETE FROM Bookmark WHERE id = :id")
    void deleteById(int id);

    @Query("DELETE FROM Bookmark WHERE name = :name")
    void deleteByName(String name);

    @Delete
    void delete(Bookmark bookmark);

    @Update
    void update(Bookmark bookmark);
}
