package cz.vaetas.ariadneheadband;

import android.arch.persistence.room.RoomDatabase;

@android.arch.persistence.room.Database(entities = {Bookmark.class}, version = 1)
public abstract class Database extends RoomDatabase {
    public abstract BookmarksDao dao();
}
