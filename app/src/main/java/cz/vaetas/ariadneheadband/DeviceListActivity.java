package cz.vaetas.ariadneheadband;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class DeviceListActivity extends AppCompatActivity {

    private static final String TAG = "Ariadne";

    BluetoothGatt gatt;
    BluetoothAdapter bluetoothAdapter;
    BluetoothDevice device;
    FloatingActionButton btnBondDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        btnBondDevice = findViewById(R.id.fab);
        btnBondDevice.setOnClickListener(view -> {
            Intent bondDeviceIntent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
            startActivity(bondDeviceIntent);
        });

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
        List<String> bondedDevicesNames = new ArrayList<>();
        for (BluetoothDevice device : bondedDevices) {
            bondedDevicesNames.add(device.getName() + " (" + device.getAddress() + ")");
        }

        // Create list of all bonded devices.
        ListAdapter deviceList = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, bondedDevicesNames);
        ListView deviceListView = findViewById(R.id.device_list);
        deviceListView.setAdapter(deviceList);

        // Create ordered list of bonded devices so we can point to them when item is clicked.
        List<BluetoothDevice> deviceArrayList = new ArrayList<>(bondedDevices);

        // Listen for device selection.
        deviceListView.setOnItemClickListener((adapterView, view, i, l) -> {
            device = bluetoothAdapter.getRemoteDevice(deviceArrayList.get(i).getAddress());
            Log.d(TAG, "onItemClick: clicked on " + device.getName());

            // If everything is right, device that we want connect to will be sent to MainActivity
            Intent returnIntent = new Intent();
            if (bluetoothAdapter != null || gatt != null || device != null) {
                returnIntent.putExtra("device", device);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else {
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

    }

}
