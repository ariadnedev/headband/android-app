package cz.vaetas.ariadneheadband;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class LocationService extends Service {

    private static final String TAG = "Ariadne";

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    public Location lastLocation;

    public LocationService() {}


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: LocationService");
        startLocationUpdates();
    }


    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = new LocationRequest()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(Utils.GPS_UPDATE_INTERVAL)
                .setFastestInterval(Utils.GPS_FASTEST_INTERVAL);

        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
    }


    LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            lastLocation = locationResult.getLastLocation();

            if (lastLocation != null) {
                Intent locationUpdatedIntent = new Intent(Utils.LOCATION_UPDATED_INTENT);
                locationUpdatedIntent.putExtra("lastLocation", lastLocation);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(locationUpdatedIntent);
            }
        }

        @Override
        public void onLocationAvailability(LocationAvailability locationAvailability) {
            super.onLocationAvailability(locationAvailability);
            if (locationAvailability.isLocationAvailable()) {
                Log.d(TAG, "onLocationAvailability: Location is available.");
                Toast.makeText(getApplicationContext(), getString(R.string.location_found), Toast.LENGTH_LONG).show();
            }
        }
    };


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: LocationService");
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification;

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    "Navigation",
                    "Navigation Channel",
                    NotificationManager.IMPORTANCE_LOW);
            notificationManager.createNotificationChannel(notificationChannel);

            notification = new Notification.Builder(this, notificationChannel.getId())
                    .setContentTitle(getString(R.string.app_running))
                    .setContentText(getString(R.string.using_gps_location))
                    .setColor(Color.CYAN)
                    .setColorized(true)
                    .setSmallIcon(R.drawable.ic_map_white_24dp)
                    .setContentIntent(pendingIntent)
                    .build();
        } else {
            notification = new Notification.Builder(this)
                    .setContentTitle(getString(R.string.app_running))
                    .setContentText(getString(R.string.using_gps_location))
                    .setSmallIcon(R.drawable.ic_map_white_24dp)
                    .setContentIntent(pendingIntent)
                    .build();
        }

        startForeground(10, notification);
        return Service.START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }
}
