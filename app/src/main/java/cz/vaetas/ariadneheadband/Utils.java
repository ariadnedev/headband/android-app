package cz.vaetas.ariadneheadband;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.util.UUID;

public class Utils {
    static long GPS_UPDATE_INTERVAL  = 5 * 1000;
    static long GPS_FASTEST_INTERVAL = 1 * 1000;

    // UUIDs for HM-10 BLE. You may need to edit these if you use different Bluetooth LE module.
    static final UUID UUID_SERVICE = UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb");
    static final UUID UUID_CHARACTERISTIC = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");

    // Broadcasts
    static String LOCATION_UPDATED_INTENT = "location_updated";
    static String BOOKMARK_SELECTED_INTENT = "bookmark_selected";

    static int getAzimuth(Location location, LatLng destination) {
        Location destLoc = new Location("");
        destLoc.setLatitude(destination.latitude);
        destLoc.setLongitude(destination.longitude);
        int azimuth = Math.round(location.bearingTo(destLoc));
        if (azimuth < 0) azimuth += 360;

        return azimuth;
    }


    static int getDistance(Location location, LatLng destination) {
        Location destLoc = new Location("");
        destLoc.setLatitude(destination.latitude);
        destLoc.setLongitude(destination.longitude);
        int distance = Math.round(location.distanceTo(destLoc));

        return distance;
    }
}
