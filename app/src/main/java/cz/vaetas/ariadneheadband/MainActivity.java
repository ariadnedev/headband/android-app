package cz.vaetas.ariadneheadband;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Ariadne";

    final static int REQUEST_SELECT_DESTINATION = 1;
    final static int SETTINGS_REQUEST = 2;
    final static int REQUEST_CONNECT_DEVICE = 3;
    final static int REQUEST_LOCATION_PERMISSION = 4;
    final static int REQUEST_ENABLE_GPS = 5;
    final static int REQUEST_BOOKMARKED_LOCATIONS = 6;

    // These variables will be taken from startActivityForResult on MapActivity
    private Location currentLocation;
    private LatLng destination;

    TextView textAzimuth;

    Button btnConnect;
    Button btnStart;
    Button btnCalibrate;
    Button btnSelectDestination;
    Button btnBookmarks;
    
    ImageView btnSettings;
    ImageView btnHelp;

    SharedPreferences sharedPreferences;

    BluetoothAdapter bluetoothAdapter;
    BluetoothDevice device;
    BluetoothGatt gatt;
    BluetoothGattService gattService;
    BluetoothGattCharacteristic gattCharacteristic;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSelectDestination = findViewById(R.id.btn_select_destination);
        btnCalibrate = findViewById(R.id.btn_calibrate);
        btnConnect = findViewById(R.id.btn_connect);
        btnStart = findViewById(R.id.btn_start);
        btnBookmarks = findViewById(R.id.btn_select_bookmarks);

        btnSettings = findViewById(R.id.btn_settings);
        btnHelp = findViewById(R.id.btn_help);

        textAzimuth = findViewById(R.id.textAzimuth);

        btnSelectDestination.setOnClickListener(view -> intentSelectDestination());

        btnCalibrate.setOnClickListener(view -> {
            Log.d(TAG, "onClick: clicked btn calibrate");
            calibrateCompass();
        });


        btnSettings.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivityForResult(intent, SETTINGS_REQUEST);
        });
        
        btnHelp.setOnClickListener(view -> {
            Log.d(TAG, "onCreate: Opening HelpActivity Activity");
            Intent intent = new Intent(MainActivity.this, HelpActivity.class);
            startActivity(intent);
        });

        btnBookmarks.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, BookmarksActivity.class);
            startActivityForResult(intent, REQUEST_BOOKMARKED_LOCATIONS);
        });

        // Check Google Play services availability
        checkServices();

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        IntentFilter gpsIntentFilter = new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION);
        registerReceiver(broadcastReceiver, gpsIntentFilter);

        // Bluetooth settings
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        IntentFilter intentFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(broadcastReceiver, intentFilter);

        if (!bluetoothAdapter.isEnabled()) {
            Toast.makeText(getApplicationContext(), R.string.bluetooth_turning_on, Toast.LENGTH_LONG).show();
            bluetoothAdapter.enable();
        }


        btnConnect.setOnClickListener(view -> {
            Log.d(TAG, "onClick: selecting bluetooth device to connect");
            if (gatt == null) {
                intentConnectDevice();
            } else if (gatt != null) {
                Log.d(TAG, "btnConnect onClick: Disconnecting from GATT.");
                Toast.makeText(this, getString(R.string.bluetooth_device_disconnected), Toast.LENGTH_LONG).show();
                gatt.disconnect();
                gatt = null;
                btnConnect.setText(getString(R.string.connect));
            }
        });

        btnStart.setOnClickListener(view -> {
            if (gatt != null) {
                if (currentLocation != null && destination != null) {
                    sendToHeadband(true, false);
                    Snackbar.make(findViewById(android.R.id.content), R.string.coordinates_sent,
                            Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), R.string.must_select_destination, Snackbar.LENGTH_LONG)
                            .setAction(R.string.select, new SelectDestinationListener())
                            .show();
                }
            } else {
                Snackbar.make(findViewById(android.R.id.content), R.string.bluetooth_not_connected, Snackbar.LENGTH_LONG)
                        .setAction(R.string.connect, new ConnectDeviceListener())
                        .show();
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

        requestLocationPermissions();

        // Register Local Broadcasts from LocationService that updates lastLocation.
        LocalBroadcastManager.getInstance(this).registerReceiver(
                broadcastReceiver,
                new IntentFilter(Utils.LOCATION_UPDATED_INTENT)
        );
    }


    private void intentSelectDestination() {
        Intent intent = new Intent(MainActivity.this, MapActivity.class);
        if (destination != null) {
            intent.putExtra("lastDestination", destination);
        }
        startActivityForResult(intent, REQUEST_SELECT_DESTINATION);
    }


    private void intentConnectDevice() {
        Intent intent = new Intent(getApplicationContext(), DeviceListActivity.class);
        startActivityForResult(intent,  REQUEST_CONNECT_DEVICE);
    }
    
    
    private class RequestPermissionsListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            requestLocationPermissions();
        }
    }


    private class SelectDestinationListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            intentSelectDestination();
        }
    }


    private class EnableBluetoothListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if (!bluetoothAdapter.isEnabled()) {
                Toast.makeText(getApplicationContext(), R.string.bluetooth_turning_on, Toast.LENGTH_LONG).show();
                bluetoothAdapter.enable();
            }
        }
    }


    private class ConnectDeviceListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            intentConnectDevice();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_SELECT_DESTINATION) {
            if (resultCode == RESULT_OK) {
//                Log.d(TAG, "onActivityResult: SELECT_LOCATION RESULT_OK Azimuth: "
//                        + getAzimuth(currentLocation, destination) + "°, Distance: " + getDistance(currentLocation, destination));
                currentLocation = Objects.requireNonNull(data).getParcelableExtra("location");
                destination = data.getParcelableExtra("destination");

                textAzimuth.setText(getString(R.string.computed_azimuth, Utils.getAzimuth(currentLocation, destination)));

                Log.d(TAG, "onActivityResult: Parcelable data arrived!");
            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "onActivityResult: RESULT_CANCELED");
            }
        }

        if (requestCode == REQUEST_CONNECT_DEVICE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                device = Objects.requireNonNull(data.getExtras()).getParcelable("device");
                Log.d(TAG, "onActivityResult: Connecting to " + Objects.requireNonNull(device).getName());
                gatt = device.connectGatt(getApplicationContext(), true, gattCallback);
            } else {
                Log.d(TAG, "onActivityResult: No device was selected");
                Snackbar.make(findViewById(android.R.id.content), R.string.bluetooth_must_select_device, Snackbar.LENGTH_LONG)
                        .setAction(R.string.select, new ConnectDeviceListener())
                        .show();
            }
        }

        if (requestCode == REQUEST_ENABLE_GPS) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG, "onActivityResult: GPS was successfully enabled. Starting LocationService...");
                startService(new Intent(this, LocationService.class));
            } else {
                Log.d(TAG, "onActivityResult: GPS not enabled.");
                Snackbar.make(findViewById(android.R.id.content), R.string.need_enable_gps, Snackbar.LENGTH_LONG)
                        .setAction(R.string.enable, view -> requestEnableGPS())
                        .show();
            }
        }

        if (requestCode == REQUEST_BOOKMARKED_LOCATIONS) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    destination = data.getParcelableExtra("bookmarkLocation");

                    if (destination != null) {
                        Log.d(TAG, "onActivityResult: Bookmark selected: " + destination.latitude + " " + destination.longitude);
                    }

                    if (destination != null && currentLocation != null) {
                        textAzimuth.setText(getString(R.string.computed_azimuth, Utils.getAzimuth(currentLocation, destination)));
                    }
                }
            }
        }
    }


    BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.d(TAG, "onConnectionStateChange: Connected to GATT client. Starting services discovery...");
                Snackbar.make(findViewById(android.R.id.content), getString(R.string.connected_to_device, device.getName()), Snackbar.LENGTH_LONG).show();
                gatt.discoverServices();
                btnConnect.setText(getString(R.string.bluetooth_disconnect));
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Toast.makeText(MainActivity.this, R.string.bluetooth_device_disconnected, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onConnectionStateChange: Disconnected from Bluetooth device.");
                btnConnect.setText(getString(R.string.connect));
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            Log.d(TAG, "onServicesDiscovered: Services discovered.");
            gattService = gatt.getService(Utils.UUID_SERVICE);
            gattCharacteristic = gattService.getCharacteristic(Utils.UUID_CHARACTERISTIC);
            gatt.setCharacteristicNotification(gattCharacteristic, true);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Log.d(TAG, "onCharacteristicChanged: Value changed to: " + gattCharacteristic.getStringValue(0));
        }
    };


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        Snackbar.make(findViewById(android.R.id.content),R.string.bluetooth_off, Snackbar.LENGTH_LONG)
                                .setAction(R.string.turn_on, new EnableBluetoothListener())
                                .show();
                        Log.d(TAG, "onReceive: Bluetooth OFF");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Toast.makeText(getApplicationContext(), R.string.bluetooth_on, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "onReceive: Bluetooth ON");
                        break;
                }
            }

            if (action.equals(LocationManager.PROVIDERS_CHANGED_ACTION)) {
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                    Log.d(TAG, "onReceive: GPS location was enabled.");
                } else {
                    Log.d(TAG, "onReceive: GPS location was disabled!");
                }
            }
            
            if (action.equals(Utils.LOCATION_UPDATED_INTENT)) {
                currentLocation = intent.getParcelableExtra("lastLocation");

                // If both currentLocation and destination are not null, final azimuth will be
                // updated on each location update.
                if (currentLocation != null && destination != null) {
                    // Log.d(TAG, "onReceive: Azimuth was updated to: " + Utils.getAzimuth(currentLocation, destination));
                    // textAzimuth.setText(getString(R.string.computed_azimuth, Utils.getAzimuth(currentLocation, destination)));
                }
            }
        }
    };


    public void checkServices() {
        Log.d(TAG, "checkServices: Checking Google Play Services");
        int googlePlayServicesAvailable = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MainActivity.this);

        if (googlePlayServicesAvailable == ConnectionResult.SUCCESS) {
            Log.d(TAG, "checkServices: Google Play Services are available.");
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(googlePlayServicesAvailable)) {
            Log.d(TAG, "checkServices: Google Play Services occurred, but is resolvable by user.");
        } else {
            Log.d(TAG, "checkServices: Google Play Services are not available.");
        }
    }


    private void requestLocationPermissions() {
        Log.d(TAG, "requestLocationPermissions: Requesting location permissions.");

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "requestLocationPermissions: Requesting permissions.");
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions(this, permissions, REQUEST_LOCATION_PERMISSION);
        } else {
            Log.d(TAG, "requestLocationPermissions: Permissions are already granted.");
            requestEnableGPS();
        }
    }


    private void requestEnableGPS() {
        LocationRequest locationRequest = new LocationRequest()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(Utils.GPS_UPDATE_INTERVAL)
                .setFastestInterval(Utils.GPS_FASTEST_INTERVAL);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());


        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);
                // All location settings are satisfied. The client can initialize location
                // requests here.
                startService(new Intent(MainActivity.this, LocationService.class));
            } catch (ApiException exception) {
                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(MainActivity.this, REQUEST_ENABLE_GPS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        } catch (ClassCastException e) {
                            // Ignore, should be an impossible error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult: ");

        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.length > 0) {
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "onRequestPermissionsResult: Permissions failed to be obtained.");
                        Snackbar.make(findViewById(android.R.id.content), R.string.need_enable_location_permissions, Snackbar.LENGTH_LONG)
                                .setAction(R.string.enable, new RequestPermissionsListener())
                                .show();
                        return;
                    }
                }

                Log.d(TAG, "onRequestPermissionsResult: Permissions granted.");
                startService(new Intent(this, LocationService.class));
            }
        }
    }


    private void calibrateCompass() {
        if (gatt != null) {
            if (destination != null && currentLocation !=  null) {
                Log.d(TAG, "calibrateCompass: Calibrating compass.");
                sendToHeadband(false, true);

                ProgressDialog calibrationDialog = new ProgressDialog(MainActivity.this);
                calibrationDialog.setMessage(getString(R.string.calibration_dialog));
                calibrationDialog.setTitle(R.string.calibration_active);
                calibrationDialog.setCancelable(false);

                calibrationDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.finish), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    sendToHeadband(false, false);
                });

                calibrationDialog.show();

            } else {
                Log.d(TAG, "calibrateCompass: no destination or currentLocation selected");
                Snackbar.make(findViewById(android.R.id.content), R.string.must_select_destination, Snackbar.LENGTH_LONG)
                        .setAction(R.string.select, new SelectDestinationListener())
                        .show();
            }
        } else {
            Log.d(TAG, "calibrateCompass: Bluetooth is not enabled or connected.");
            Snackbar.make(findViewById(android.R.id.content), R.string.must_be_connected, Snackbar.LENGTH_LONG)
                    .setAction(R.string.connect, new ConnectDeviceListener())
                    .show();
        }
    }


    private void sendToHeadband(boolean startNavigating, boolean startCalibrating) {

        byte[] data = new byte[7];

        short azimuth = (short) Utils.getAzimuth(currentLocation, destination);
        data[0] = (byte)(azimuth & 0xff);
        data[1] = (byte)((azimuth >> 8) & 0xff);

        short distance = (short) Utils.getDistance(currentLocation, destination);
        data[2] = (byte)(distance & 0xff);
        data[3] = (byte)((distance >> 8) & 0xff);

        String intensityPref =  sharedPreferences.getString("intensity", "255");
        int intensity = Integer.parseInt(intensityPref);
        data[4] = (byte) intensity;

        if (startNavigating) {
            data[5] = (byte) 1;
        } else {
            data[5] = (byte) 0;
        }

        if (startCalibrating) {
            data[6] = (byte) 1;
        } else {
            data[6] = (byte) 0;
        }

        Log.d(TAG, "sendToHeadband: SENDING"
                + " | azimuth: "          + azimuth
                + " | distance: "         + distance
                + " | intensity: "        + intensity
                + " | startNavigating: "  + data[5]
                + " | startCalibrating: " + data[6]
        );

        gattCharacteristic.setValue(data);
        gatt.writeCharacteristic(gattCharacteristic);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (gatt != null) {
            Log.d(TAG, "onDestroy: Disconnecting from GATT server.");
            gatt.disconnect();

            unregisterReceiver(broadcastReceiver);
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        }
    }
}
