package cz.vaetas.ariadneheadband;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

class BookmarksAdapter extends android.support.v7.widget.RecyclerView.Adapter<BookmarksAdapter.ViewHolder> {

    private static final String TAG = "Ariadne";

    Context context;
    List<Bookmark> bookmarks;
    Database database;

    public BookmarksAdapter(List<Bookmark> bookmarks, Context context) {
        this.bookmarks = bookmarks;
        this.context = context;
    }

    @Override
    public BookmarksAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bookmark_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookmarksAdapter.ViewHolder viewHolder, int i) {
        viewHolder.name.setText(bookmarks.get(i).getName());

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(',');
        DecimalFormat df = new DecimalFormat("#.000000", otherSymbols);

        viewHolder.latitude.setText(
                context.getString(R.string.bookmark_item_lat, df.format(bookmarks.get(i).getLatitude()))
        );

        viewHolder.longitude.setText(
                context.getString(R.string.bookmark_item_lng, df.format(bookmarks.get(i).getLongitude()))
        );
    }

    @Override
    public int getItemCount() {
        return bookmarks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView latitude;
        public TextView longitude;
        public ImageView btnEdit;
        public ImageView btnRemove;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            database = Room.databaseBuilder(context, Database.class, "bookmarks")
                    .allowMainThreadQueries()
                    .build();

            this.name = itemView.findViewById(R.id.name);
            this.latitude = itemView.findViewById(R.id.latitude);
            this.longitude = itemView.findViewById(R.id.longitude);
            this.btnEdit = itemView.findViewById(R.id.btn_edit);
            this.btnRemove = itemView.findViewById(R.id.btn_remove);

            itemView.setOnClickListener(view -> {
                Log.d(TAG, "onClick: clicked = " + bookmarks.get(getAdapterPosition()).getName());

                LatLng bookmarkLocation = new LatLng(
                        bookmarks.get(getAdapterPosition()).getLatitude(),
                        bookmarks.get(getAdapterPosition()).getLongitude());

                Intent locationUpdatedIntent = new Intent(Utils.BOOKMARK_SELECTED_INTENT);
                locationUpdatedIntent.putExtra("bookmarkLocation", bookmarkLocation);
                LocalBroadcastManager.getInstance(context).sendBroadcast(locationUpdatedIntent);
            });

            itemView.setOnLongClickListener(view -> {
                Log.d(TAG, "onLongClick: DELETING " + bookmarks.get(getAdapterPosition()).getName());
                database.dao().deleteByName(bookmarks.get(getAdapterPosition()).getName());
                return true;
            });

            btnEdit.setOnClickListener(view -> {
                Log.d(TAG, "ViewHolder: btnEdit clicked: " + getAdapterPosition() + " " + bookmarks.get(getAdapterPosition()).getId());

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(context.getString(R.string.rename));
                View viewInflated = LayoutInflater.from(context)
                        .inflate(R.layout.dialog_edit_bookmark, (ViewGroup) view.getParent(), false);

                final EditText input = viewInflated.findViewById(R.id.input);
                input.setText(bookmarks.get(getAdapterPosition()).getName());
                builder.setView(viewInflated);


                builder.setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    Log.d(TAG, "onClick: " + input.getText().toString());

                    // Create new object for updated bookmark.
                    Bookmark updatedBookmark = bookmarks.get(getAdapterPosition());
                    // And change its name to new name from text input.
                    updatedBookmark.setName(input.getText().toString());

                    // Then update database object with new data.
                    database.dao().update(updatedBookmark);

                    // Notify local list that the name was changed.
                    bookmarks.set(getAdapterPosition(), updatedBookmark);
                    notifyItemChanged(getAdapterPosition());

                    // Inform user about renaming success.
                    Snackbar.make((ViewGroup) view.getParent(), context.getString(R.string.bookmark_renamed), Snackbar.LENGTH_SHORT).show();
                });

                builder.setNegativeButton(android.R.string.cancel,
                        (dialogInterface, i) -> dialogInterface.cancel());

                builder.show();
            });

            btnRemove.setOnClickListener(view -> {
                Log.d(TAG, "ViewHolder: btnRemove clicked: " + getAdapterPosition());

                database.dao().delete(bookmarks.get(getAdapterPosition()));

                bookmarks.remove(getAdapterPosition());
                notifyItemRemoved(getAdapterPosition());

                Snackbar.make((ViewGroup) view.getParent(), context.getString(R.string.bookmark_removed), Snackbar.LENGTH_SHORT).show();
            });
        }
    }
}