package cz.vaetas.ariadneheadband;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;


public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "Ariadne";
    private static final int LOCATION_PERMISSION_REQUEST = 1;
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 2;
    private boolean isLocationPermissionGranted = false;
    private static final int DEFAULT_MAP_ZOOM = 15;

    private GoogleMap map;

    // Users current location and chosen destination will be sent back to MainActivity
    private Location currentLocation;
    private LatLng destination;
    private String destinationName;

    // Need to deliver only local suggestions with Google Places Autocompletion SDK.
    String userCountryCode;


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady: Map is ready");
        map = googleMap;

        if (isLocationPermissionGranted) {
            getDeviceLocation();

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            map.setMyLocationEnabled(true);
        }

        map.setOnMapClickListener(latLng -> placeMarker(latLng, getString(R.string.manual_destination)));

        // If the user closes MapActivity and opens it again, he will see his last placed marker.
        LatLng lastDestination = getIntent().getParcelableExtra("lastDestination");
        if (lastDestination != null) {
            Log.d(TAG, "onMapReady: Using last selected destination");
            placeMarker(lastDestination, getString(R.string.last_selected_destination));
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // Action toolbar setting.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.select_destination));

        // Google Place Autocomplete that allows to quickly find local places.
        ImageView btnSearchLocation = findViewById(R.id.ic_search_location);
        btnSearchLocation.setOnClickListener(view -> {
            try {
                AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                        .setCountry(userCountryCode)
                        .build();

                Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                        .setFilter(typeFilter)
                        .build(MapActivity.this);

                Log.d(TAG, "onClick: opened Google Place Autocomplete");
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                Log.e(TAG, "onClick: GooglePlayServicesRepairableException", e);
            } catch (GooglePlayServicesNotAvailableException e) {
                Log.e(TAG, "onClick: GooglePlayServicesNotAvailableException", e);
            }
        });


        // Get users country code to filter Place Autocomplete results. While this method
        // needs a SIM card to work, this is not problem because you already need GPS and Mobile
        // internet in order to use Ariadne Headband fully.
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
            userCountryCode = telephonyManager.getNetworkCountryIso();
        } catch (NullPointerException e) {
            Log.e(TAG, "onCreate: ", e);
            userCountryCode = "us";
        }

        // Floating button to confirm save selected destination and current location and send ti
        // back to MainActivity so we can send it to Arduino.
        FloatingActionButton btnConfirmDestination = findViewById(R.id.btn_confirm_destination);
        btnConfirmDestination.setOnClickListener(view -> {
            if (destination == null) {
                Snackbar.make(findViewById(android.R.id.content), "You must select destination!", Snackbar.LENGTH_SHORT).show();
            } else if (currentLocation == null) {
                Snackbar.make(findViewById(android.R.id.content), "Unable to get your position!", Snackbar.LENGTH_SHORT).show();
            } else {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                returnIntent.putExtra("location", currentLocation)
                        .putExtra("destination", destination)
                        .putExtra("destinationName", destinationName);
                finish();
            }
        });

        // Broadcast receiver to watch GPS state changes in order to inform users (to turn it on).
        registerReceiver(gpsStateReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));

        getLocationPermissions();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.d(TAG, "Place: " + place);
                moveCamera(place.getLatLng(), DEFAULT_MAP_ZOOM);
                placeMarker(place.getLatLng(), place.getName().toString());
                destination = place.getLatLng();
                destinationName = (String) place.getName();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "onActivityResult: canceled Place Autocomplete search");
            }
        }
    }


    private void initMap() {
        Log.d(TAG, "initMap: Initializing map");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapActivity.this);
    }


    private void getLocationPermissions() {
        Log.d(TAG, "getLocationPermissions: Getting location permissins.");

        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "getLocationPermissions: Permissions are granted.");
            isLocationPermissionGranted = true;
            initMap();
        } else {
            Log.d(TAG, "getLocationPermissions: Requesting permissions.");
            ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult: Triggered.");

        if (requestCode == LOCATION_PERMISSION_REQUEST) {
            if (grantResults.length > 0) {
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "onRequestPermissionsResult: Permissions failed to be obtained.");
                        return;
                    }
                }

                Log.d(TAG, "onRequestPermissionsResult: Permissions granted");
                isLocationPermissionGranted = true;
                initMap();
            }

        }
    }
    
    
    private void getDeviceLocation() {
        Log.d(TAG, "getDeviceLocation: Getting device location.");
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            if (isLocationPermissionGranted) {
                Task location = fusedLocationProviderClient.getLastLocation();

                location.addOnCompleteListener(task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        Log.d(TAG, "onComplete: Location was found.");
                        currentLocation = (Location) task.getResult();
                        moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                                DEFAULT_MAP_ZOOM);
                    } else {
                        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            Log.d(TAG, "onComplete: GPS in not on.");
                            Snackbar.make(findViewById(android.R.id.content), R.string.turn_gps_on, Snackbar.LENGTH_LONG).show();
                        } else {
                            Log.d(TAG, "onComplete: Cannot find current location.");
                            Snackbar.make(findViewById(android.R.id.content), "Trying to find your location...", Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
            }
        } catch (SecurityException exception) {
            Log.e(TAG, "getDeviceLocation: SecurityException" + exception.getMessage());
        }
    }


    private void moveCamera(LatLng latLng, float zoom) {
        Log.d(TAG, "moveCamera: Moving camera.");
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }
    

    private void placeMarker(LatLng latLng, String title) {
        map.clear();
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(title);
        map.addMarker(markerOptions);
        destination = latLng;
    }
    

    @Override
    public boolean onSupportNavigateUp() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
        return true;
    }
    

    private BroadcastReceiver gpsStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Log.d(TAG, "onReceive: now is gps off");
                    Snackbar.make(findViewById(android.R.id.content), R.string.turn_gps_on, Snackbar.LENGTH_LONG).show();
                } else {
                    Log.d(TAG, "onReceive: now is gps on");
                }

            }
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(gpsStateReceiver);
    }
}