package cz.vaetas.ariadneheadband;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;

import java.util.Objects;


public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = "Ariadne";

    ImageView btnSaveSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_settings);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, new SettingsFragment())
                .commit();

        btnSaveSettings = findViewById(R.id.ic_save_settings);
        btnSaveSettings.setOnClickListener(view -> {
            Log.d(TAG, "onClick: saving settings and finishing activity");
            finish();
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
        Log.d(TAG, "onSupportNavigateUp: clicked");
        return true;
    }
}
