package cz.vaetas.ariadneheadband;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class BookmarksActivity extends AppCompatActivity {

    private static final String TAG = "Ariadne";

    private final int REQUEST_ADD_BOOKMARK = 11;

    List<Bookmark> bookmarks;

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    Database database;

    FloatingActionButton btnAddBookmark;

    private LatLng destination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmarks);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        database = Room.databaseBuilder(getApplicationContext(), Database.class, "bookmarks")
                .allowMainThreadQueries()
                .build();

        bookmarks = database.dao().getAll();
        Log.d(TAG, "onCreate: " + bookmarks);

        recyclerView = findViewById(R.id.bookmarks_list);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new BookmarksAdapter(bookmarks, BookmarksActivity.this);
        recyclerView.setAdapter(adapter);

        btnAddBookmark = findViewById(R.id.btn_add_bookmark);
        btnAddBookmark.setOnClickListener(view -> {
            Intent intent = new Intent(BookmarksActivity.this, MapActivity.class);
            startActivityForResult(intent, REQUEST_ADD_BOOKMARK);
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(
                broadcastReceiver,
                new IntentFilter(Utils.BOOKMARK_SELECTED_INTENT)
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        if (requestCode == REQUEST_ADD_BOOKMARK) {
            if (resultCode == RESULT_OK) {
                destination = data.getParcelableExtra("destination");

                String destinationName = data.getStringExtra("destinationName");

                if (destinationName == null) {
                    Geocoder gcd = new Geocoder(BookmarksActivity.this, Locale.getDefault());
                    List<Address> addresses;
                    try {
                        Log.d(TAG, "onActivityResult: Setting bookmark name from Geocoder.");
                        addresses = gcd.getFromLocation(destination.latitude, destination.longitude, 1);
                        destinationName = addresses.get(0).getAddressLine(0);

                        // This might not be the best solution... Don't judge me!
                        destinationName = destinationName.substring(0, destinationName.indexOf(","));
                    } catch (IOException e) {
                        Log.e(TAG, "onActivityResult: ", e);
                        destinationName = getString(R.string.unnamed_bookmark);
                    }
                }

                // Create new Bookmark object
                Bookmark newBookmark = new Bookmark(destinationName, destination.latitude, destination.longitude);

                // Insert new bookmark into database
                database.dao().insert(newBookmark);

                // Clear bookmarks list data instead of just updating it because life is short.
                bookmarks.clear();
                bookmarks.addAll(database.dao().getAll());
                adapter.notifyDataSetChanged();

                Snackbar.make(findViewById(android.R.id.content), getString(R.string.bookmark_added), Snackbar.LENGTH_SHORT).show();
            } else {
                Log.d(TAG, "onActivityResult: CANCELLED");
            }
        }
    }

    LatLng bookmarkLocation;
    
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            
            if (action.equals(Utils.BOOKMARK_SELECTED_INTENT)) {
                bookmarkLocation = intent.getParcelableExtra("bookmarkLocation");
                endActivity(Activity.RESULT_OK);
            }
        }
    };

    void endActivity(int result) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("bookmarkLocation", bookmarkLocation);
        setResult(result, returnIntent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }
}
