# Ariadne Headband Android Control App

> Ariadne Headband is ongoing project that aims to create haptic navigation for blind people (among 
others). It uses 4 vibration motors placed in set directions on head and connected to Arduino board. 
Everything is controlled with Android app via Bluetooth. You can view our [Hackaday page](https://hackaday.io/project/160367-ariadne-headband)
to get more informations.

This repository contains our Android app that serves as a control centre for *Ariadne Headband*. Using 
this app, you can choose destination, find your current location and start navigating.

![Screenshots from this app](screenshots.png)

## Milestones
This app is still under development and not everything works correctly or is already implemented. Below are written some milestones (or simply *bigger* tasks) we are working on.


**General**
- [ ] Remove the Location requirement when adding new bookmark.
- [ ] Run database queries on different thread.
- [x] When creating task, infer the name from LatLng or Location object of place.

**Location**
- [x] Add LocationService class that will track user position.

**Bluetooth**
- [x] Switch to Bluetooth Low Energy and use native libraries.


## API keys
This app uses Google Maps SDK. In order to build this project you will need your own Google API key
(maps will not work without it). You can obtain one at [Google Cloud Console APIs](https://console.cloud.google.com/apis/).
Here, you must enable `Maps SDK for Android` for maps, and `Places SDK for Android ` for place search
autocompletion.

When you have your API key with everything enabled, add following line to `strings.xml`. You can also 
place it inside different file, for example `secrets.xml` so you can hide it from Git.

```xml
<string name="google_maps_api">YOUR_API_KEY</string>
```

## License
```
Ariadne Headband Android Control App
Copyright (C) 2018  Vojtěch Pavlovský

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```